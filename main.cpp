#include <iostream>
#include <vector>
#include "kmeans.h"

template <typename T> //よくわからない御子貝が生み出した乱数
T GetRandom()
{
    T min = 0.0;
    T max = 1000.0;
    return min + (T)(rand()*(max - min + 1.0) / (1.0 + RAND_MAX));
}

int main() { //メインです



    int krst_n, n;
    kmeans kmeans1;
    std::vector <DataPoint> point; //散りばめられる点
    std::vector <KrstPoint> kpoint; //クラスタの重心になったりするやつ

    std::cout << "yousosuu:";
    std::cin >> n;
    std::cout << "kurasutasuu:";
    std::cin >> krst_n;

    for (int i = 0; i < krst_n; i++) {
        kpoint.push_back(KrstPoint(GetRandom<int>(),
                                   GetRandom<int>(),
                                   GetRandom<int>(), i));
    }
    for (int i = 0; i < n; i++) {
        point.push_back(DataPoint(GetRandom<int>(),
                                  GetRandom<int>(),
                                  GetRandom<int>(), -1));
    }

    run(krst_n, n, kpoint, point);

    return 0;
}