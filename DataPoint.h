//
// Created by yamany on 2018/12/29.
//

#ifndef UNTITLED17_DATAPOINT_H
#define UNTITLED17_DATAPOINT_H

#include "math.h"

struct DataPoint { //座標とクラスタの管理

    int _x;
    int _y;
    int _z;
    int _krst;

    DataPoint(int x, int y, int z, int krst) :_x(x), _y(y), _z(z), _krst(krst) {}
};

struct KrstPoint :public DataPoint { //クラスタ用クラス。分けたほうがわかりやすいかなって分けたけど今思えば特に意味はない
    using DataPoint::DataPoint;
    double distance(DataPoint p) { return (pow(_x - p._x, 2.0) + pow(_y - p._y, 2.0) + pow(_z - p._z, 2.0)); };

};


#endif //UNTITLED17_DATAPOINT_H
