//
// Created by yamany on 2018/12/29.
//

#ifndef UNTITLED17_KMEANS_H
#define UNTITLED17_KMEANS_H

#include "kmeans.h"
#include <iostream>
#include <vector>
#include "DataPoint.h"


class kmeans {
    std::vector <KrstPoint> _kpoint;
    std::vector <DataPoint> _dpoint;
    
    kmeans(std::vector <KrstPoint> kpoint, std::vector <DataPoint> dpoint) :_kpoint(kpoint), _dpoint(dpoint){};

    void center_of_gravity(KrstPoint &p1, std::vector <DataPoint> &p2) { //センター・オブ・ジ・アースみたいな名前の重心を求めるやつ。物理noobなので式があってるかわからないからよろしく
        int x_sum = 0, y_sum = 0, z_sum = 0;
        for (unsigned int i = 0; i < p2.size(); i++) {
            x_sum += p2[i]._x;
            y_sum += p2[i]._y;
            z_sum += p2[i]._z;
        }

        p1._x = x_sum / p2.size();
        p1._y = y_sum / p2.size();
        p1._z = z_sum / p2.size();

    }

    void grouping_krst(int krst_n, int n) { //これで一般点をpointをクラスタに属させる
        int min = 0;
        double tmp = 0.0;
        for (int i = 0; i < n; i++) {
            min = 0;
            tmp = 0.0;
            for (int j = 0; j < krst_n; j++) {
                tmp = _kpoint[j].distance(_dpoint[i]);
                if (_kpoint[min].distance(_dpoint[i]) > tmp) min = j;
            }
            _dpoint[i]._krst = _kpoint[min]._krst;

        }
    }

    bool check_finish(std::vector <KrstPoint> p1, std::vector <KrstPoint> p2) { //もうクラスタの移動がないかどうかの確認
        unsigned int i, cnt=0;
        bool flg = false;
        for (i = 0; i < p1.size(); i++) {
            if (p1[i]._x == p2[i]._x && p1[i]._y == p2[i]._y && p1[i]._z == p2[i]._z) cnt++;
        }
        if(i==cnt) flg = true;
        else flg = false;

        return flg;
    }
public:
    void run(int krst_n, int n) { //メインです
        std::vector <DataPoint> tmp; //後で使います
        std::vector <KrstPoint> kpoint_tmp; //前回の重心との差分を見るために用意

        do {
            grouping_krst(krst_n, n);
            for (int i = 0; i < krst_n; i++) {
                for (int j = 0; j < n; j++) {
                    if (_dpoint[j]._krst == _kpoint[i]._krst) {
                        tmp.push_back(DataPoint(_dpoint[j]._x, //頭が悪いので1つのクラスタごとに全ての要素をぶん回して確認、配列格納。
                                                _dpoint[j]._y,
                                                _dpoint[j]._z,
                                                _dpoint[j]._krst));
                    }
                }
                center_of_gravity(_kpoint[i], tmp);
                tmp.clear();
            }
            kpoint_tmp = _kpoint;
        }while (!check_finish(_kpoint, kpoint_tmp));
    }
};


#endif //UNTITLED17_KMEANS_H
